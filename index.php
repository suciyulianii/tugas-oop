<?php
    require('Animal.php');
    require('Frog.php');
    require('Ape.php');

    $sheep = new Animal("shaun");

    echo "Nama hewan : $sheep->name <br>"; // "shaun"
    echo "Legs : $sheep->legs <br>"; // 2
    echo "Cold blooded : $sheep->cold_blooded <br><br>"; // false

    $sungokong = new Ape("kera sakti");
    echo "Nama hewan : $sungokong->name <br>"; 
    echo "Legs : $sungokong->legs <br>"; 
    echo "Cold blooded : $sungokong->cold_blooded <br>"; 
    $sungokong->yell() ; // "Auooo"
    echo "<br><br>";

    $kodok = new Frog("buduk");
    echo "Nama hewan : $kodok->name <br>"; 
    echo "Legs : $kodok->legs <br>"; 
    echo "Cold blooded : $kodok->cold_blooded <br>"; 
    $kodok->jump() ; // "hop hop"
